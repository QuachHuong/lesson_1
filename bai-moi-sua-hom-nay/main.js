// bai1
const handleSalary = money =>{
    var luong;
    if ( money >= 15 ){
        luong = money - money * 0.3;
        return luong;
    }
    if ( money < 15 && money > 7 ){
        luong = money - money * 0.2;
        return luong;
    }
    if ( money <= 7 && money > 0){
        luong = money - money * 0.1;
        return luong;
    }
}
// bai2
const handleAge = age => {
    if ( age < 16 ){
        console.log('chưa đủ tuổi vào lớp 10');
        return;
    }
    else {
        console.log('đã đủ tuổi vào lớp 10');
    }
}
// bai 3
const handleCompare = number =>{
    if ( number > 100 ) {
        console.log('số ' +number+ ' là số lớn hơn 100');
        return;
    }
    else {
        console.log('số ' +number+ ' là số nhỏ hơn 100');
    }
}
// bai 4
const handleMinmax = (num1, num2, num3) =>{
    var max = num1;
    if ( max < num2 ){
        return max = num2;
    }
    if ( max < num3 ){
        return max = num3;
    }
}
// bai 5
const handleAcademic = (num1, num2, num3) =>{
    var ave = (num1 + num2 + num3) / 3;
    if ( ave >= 9){
        console.log('hạng A');
        return;
    }
    else if ( ave >=7 && ave < 9){
        console.log('hạng B');
        return;
    }
    else if ( ave >=5 && ave < 7){
        console.log('hạng C');
        return;
    }
    else {
        console.log('hạng F');
        return;
    }
}
// bai 6
const handleMath = (a,b,c) =>{
    var delta = b * b -4 * a * c;
    var x1, x2;
    if ( a === 0 && b === 0 ) {
        console.log('phương trình vô nghiệm');
        return;
    }
    if ( a === 0 ) {
        x1 = -(c/b);
        console.log('phương trình có 1 nghiệm' + x1 );
        return;
    }
    if (delta < 0)  {
        console.log('phương trình vô nghiệm');
        return;
    }
    if (delta > 0)  {
        x1 = ((-b + Math.sqrt(delta)) / (2*a));
        x2 = ((-b - Math.sqrt(delta)) / (2*a));
        console.log('phương trình có 2 nghiệm: ' + x1 + ' ' + x2 );
        return;
    }

}
// bai 7
const handleCommission = money =>{
    if ( money <= 100){
        return money * 0.05;
    }
    if ( money <= 300 && money > 100){
        return money * 0.1;
    }
    if ( money > 300){
        return money * 0.2;
    }
}
// bai 8
const handlePostage = minute =>{
    var fee = 25000;
    if ( minute < 50 ) {
        return fee + minute * 600;
    }
    if ( minute <= 200 ) {
        return fee + ( minute - 50 ) * 400 + 50 * 600; 
    }
    if ( minute > 200) {
        return fee + 150 * 400 + 50 * 600 + ( minute - 200 ) * 200;
    }
}