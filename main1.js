/** Toán tử gán
 * 
 * Toán tử      Ví dụ           Tương đương
 * =            x = y           x = y
 * +=           x += y          x = x + y
 * -=           x -= y          x = x - y
 * *=           x *= y          x = x * y
 * /=           x /= y          x = x / y
 * **=          x **= y         x = x ** y
 */

// var a = 1;

// a **= 2;

// console.log(a);


/**
 * Toán tử chuỗi - string oparator
 * 
 */

var firstName = 'Huong';
var lastName = 'Quach';

console.log(firstName + ' ' + lastName);

/** Toán tử so sánh
 * Toán tử
 * ==       --> Bằng
 * !=       --> không bằng
 * >        --> lớn hơn
 * <        --> nhỏ hơn
 * >=       --> lớn hơn hoặc bằng
 * <=       --> nhỏ hơn hoặc bằng
 * 
 */

var a = 1;
var b = 2;

if(a != b){
    console.log('điều kiện đúng !');
} else{
    console.log('Điều kiện sai');

}

/**
 * Boolean
 */
var a = 1;
var b = 2;
var isSuccess = a > b;
console.log(isSuccess);

/**
 * if - else
 */
/** 6 giá trị sau convert sang boolean sẽ là false, khác 6 giá trị này thì khi convert sang boolean thì sẽ là true
 * 
 * 0
 * false
 * '' hoặc ""
 * undefined
 * NaN
 * null
 */
var isSuccess = 1 < 2;

if (isSuccess) {
    console.log('Điều kiện đúng');
} else {
    console.log('Điều kiện sai');
}

/**
 * Toán tử logic
 * 1. && - and
 * 2. || - or
 * 3. ! - not
 */

var a= 1;
var b = 2;
var c = 3;
if (a > 0 || b < 0) {
    console.log('điều kiện đúng!');
}
if (a > 0 && b < 0) {
    console.log('điều kiện đúng!');
}
if (!(a > 0) ) {
    console.log('điều kiện đúng!');
}
